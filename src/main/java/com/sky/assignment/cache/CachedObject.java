package com.sky.assignment.cache;

import java.util.Date;

import org.joda.time.DateTime;

public class CachedObject<T> {

	private T obj;
	private Date expiryDate;
	
	/**
	 * Private default constructor
	 */
	@SuppressWarnings("unused")
	private CachedObject() {
		
	}

	/**
	 * 
	 * @param obj object to cache
	 * @param secondsToExpire length of time this object is valid in cache
	 */
	public CachedObject(T obj, int secondsToExpire) {
		this.obj = obj;
		this.expiryDate = (new DateTime()).plusSeconds(secondsToExpire).toDate();
	}
	
	
	/**
	 * Get Object if it's not expired
	 * @return object if not expired, otherwise null
	 */
	public T getObj() {
		if(this.expiryDate.after(new DateTime().toDate())) {
			// not yet expired
			return this.obj;
		} else {
			return null;
		}
	}
	
}
