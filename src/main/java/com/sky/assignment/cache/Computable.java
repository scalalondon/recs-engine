package com.sky.assignment.cache;

public interface Computable<A,V> {
	
	/**
	 * Compute V given A, cache indefinitely
	 * 
	 * @param arg
	 * @return
	 */
	V compute(A arg) throws InterruptedException;
	
}
