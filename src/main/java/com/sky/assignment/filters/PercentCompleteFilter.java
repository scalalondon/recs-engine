package com.sky.assignment.filters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sky.assignment.model.Recommendation;

@Component
public class PercentCompleteFilter implements RecFilter {

	public final Integer percentageCompleteThreshold;

	@Autowired
	public PercentCompleteFilter(Integer percentageCompleteThreshold) {
		this.percentageCompleteThreshold = percentageCompleteThreshold;
	}
	
    @Override
    public boolean isRelevant(Recommendation r, long start, long end) {
        // this filter should discard recommendations that running time is past 60%
        // for example if recommendation start time is 8:00 and end time is 9:00 then
        // this recommendation should be discarded if timeslot start is past 8:36, which is 60% of total show time
    	double recommendationOkThresholdTime = r.start + ((double)(r.end - r.start) / 100) * 60;
    	return (recommendationOkThresholdTime >= start && recommendationOkThresholdTime <= r.end) ? true : false;
    }
}
