package com.sky.assignment;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.sky.assignment.filters.PercentCompleteFilter;
import com.sky.assignment.filters.RecFilter;
import com.sky.assignment.model.Recommendation;
import com.sky.assignment.model.Recommendations;

public class RecsEngineTest {

	private RecsEngine recsEngine;
	
	@Before
	public void setup() {
		Integer percentageCompleteThreshold = 60;
		RecFilter percentCompleteRecFilter = new PercentCompleteFilter(percentageCompleteThreshold);
		recsEngine = new RecsEngine(new RecFilter[] {percentCompleteRecFilter});
	}
	
	@Test
	public void testRecommend() {
		int numberOfRecs = 10;
		DateTime now = new DateTime();
		final Recommendations recommendations = recsEngine.recommend(numberOfRecs, now.getMillis(), now.plusMinutes(30).getMillis());
		assertEquals(numberOfRecs, recommendations.recommendations.size());
	}

	@Test
	public void testGenerate() {
		final Recommendation recommendation = recsEngine.generate();
		assertNotNull(recommendation.uuid);
		assertNotNull(recommendation.end);
		assertNotNull(recommendation.start);
		assertTrue(recommendation.end > recommendation.start);
	}

}
