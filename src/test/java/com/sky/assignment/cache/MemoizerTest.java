package com.sky.assignment.cache;

import static org.junit.Assert.*;

import org.junit.Test;

public class MemoizerTest {

	@Test
	public void testMemozer() {
		Cache cache = new Cache();
		String testString = "abc";
		assertEquals(testString+testString,cache.service(testString));
		// contrived even though cache.service keeps appending testString on each call
		assertEquals(testString+testString,cache.service(testString));
		assertEquals(testString+testString,cache.service(testString));
	}
	
	
	static class Cache {
		private final Computable<String,String> c = new Computable<String, String>() {
			
			/**
			 * contrived compute function - Side effects mutating - but just to check cache always returns compute of first value used.
			 */
			@Override
			public String compute(String arg) throws InterruptedException {
				
				return arg+arg;
			}
		};
		
		private final Computable<String, String> memoizer = new Memoizer<>(c);
		
		public String service(String arg) {
			try {
				return memoizer.compute(arg);
			} catch(InterruptedException e) {
				return "compute interrupted";
			}
		}
		
	}

	
}
