package com.sky.assignment.cache;

import static org.junit.Assert.*;

import org.junit.Test;

public class CachedObjectTest {

	@Test
	public void testGetObj() throws InterruptedException {
		
		int secondsToExpire = 1;
		String obj = "hello";
		CachedObject<String> cachedObject = new CachedObject<String>(obj, secondsToExpire);
		assertEquals(obj, cachedObject.getObj());
		Thread.sleep(1020);
		assertNull(cachedObject.getObj());
		
	}

}
