package com.sky.assignment;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.support.GenericApplicationContext;

public class SpringConfigTest {

	@Test
	public void testDispatcherServlet() {
		SpringConfig springConfig  = new SpringConfig();
		GenericApplicationContext ctx = new GenericApplicationContext();
		ServletRegistrationBean servletRegistrationBean = springConfig.dispatcherServlet(ctx);
		assertEquals("dispatcherServlet",servletRegistrationBean.getServletName());
		assertEquals(1, servletRegistrationBean.getUrlMappings().size());
		assertEquals("/*", servletRegistrationBean.getUrlMappings().iterator().next());
		assertEquals(SpringConfig.PERCENTAGE_COMPLETE_THRESHOLD, springConfig.percentageCompleteThreshold());
	}
	

}
