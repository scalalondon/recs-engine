package com.sky.assignment;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.sky.assignment.filters.PercentCompleteFilter;
import com.sky.assignment.filters.RecFilter;
import com.sky.assignment.model.Recommendations;

public class RecsControllerTest {

	private RecsController recsController;
	
	@Before
	public void setup() {
		Integer percentageCompleteThreshold = 60;
		RecFilter recFilter = new PercentCompleteFilter(percentageCompleteThreshold);
		RecFilter[] filters = new RecFilter[] {recFilter};
		RecsEngine recsEngine = new RecsEngine(filters);
		recsController = new RecsController(recsEngine);		
	}
	
	@Test
	public void testGetPersonalisedRecommendations() {
		long numberOfRecs = 10;
		DateTime now = new DateTime();
		String subscriber = "asd";
		Recommendations recommendations = recsController.getPersonalisedRecommendations(numberOfRecs, now.getMillis(), now.plusMinutes(30).getMillis(), subscriber);
		assertEquals(10, recommendations.recommendations.size());
	
	}

}
