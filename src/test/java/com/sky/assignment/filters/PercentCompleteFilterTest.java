package com.sky.assignment.filters;

import com.sky.assignment.model.Recommendation;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test {@link PercentCompleteFilter}
 */
public class PercentCompleteFilterTest {

    private PercentCompleteFilter percentCompleteFilter;
    private int percentCompleteThreshold = 60;
    
    @Before
    public void setup() {
    	percentCompleteFilter = new PercentCompleteFilter(percentCompleteThreshold);
    }

    /**
     * Test various combinations including boundary conditions. Use simple values for time window.
     * 
     * @throws Exception
     */
    @Test
    public void testThatFilterPassesAll() throws Exception {
        String uuid = "1";
        Long programStartTime = 50L;
        Long programEndTime = 80L; // 60% = 68
        Recommendation r = new Recommendation(uuid, programStartTime, programEndTime);
        assertFalse(percentCompleteFilter.isRelevant(r, 69, 99));
        r = new Recommendation(uuid, programStartTime, programEndTime);
        assertTrue(percentCompleteFilter.isRelevant(r, 68, 98));
        r = new Recommendation(uuid, programStartTime, programEndTime);
        assertTrue(percentCompleteFilter.isRelevant(r, 67, 98));
        r = new Recommendation(uuid, programStartTime, programEndTime);
        assertTrue(percentCompleteFilter.isRelevant(r, 50, 80));
        r = new Recommendation(uuid, programStartTime, programEndTime);
        assertTrue(percentCompleteFilter.isRelevant(r, 38, 68));
        r = new Recommendation(uuid, programStartTime, programEndTime);
        assertFalse(percentCompleteFilter.isRelevant(r, 81, 91));
    }

}
