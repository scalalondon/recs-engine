package com.sky.assignment.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class RecommendationsTest {

	@Test
	public void testConstructor() {
		List<Recommendation> recommendations = new ArrayList<Recommendation>();
		assertEquals(recommendations, (new Recommendations(recommendations)).recommendations);
	}

}
