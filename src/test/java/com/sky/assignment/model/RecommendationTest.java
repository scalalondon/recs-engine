package com.sky.assignment.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RecommendationTest {

	private Recommendation recommendation;
	private String uuid = "random uuid";
	private Long start = 1L;
	private Long end = 2L;	
	
	@Before
	public void setup() {
		recommendation = new Recommendation(uuid, start, end);	
	}
	
	@Test
	public void testRecommendation() {
		assertEquals(uuid, recommendation.uuid);
		assertEquals(start, recommendation.start);
		assertEquals(end, recommendation.end);
	}

	@Test
	public void testHashCodeEquals() {
		Recommendation recommendation2 = new Recommendation(uuid, start, end);
		assertEquals(new Integer(recommendation.hashCode()), new Integer(recommendation2.hashCode()));
		
		recommendation2 = new Recommendation("other uuid", start, end);
		assertFalse(recommendation2.hashCode() == recommendation.hashCode());
		
	}

}
